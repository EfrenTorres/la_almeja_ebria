-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 02-04-2020 a las 06:37:43
-- Versión del servidor: 10.4.8-MariaDB
-- Versión de PHP: 7.3.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `bdweb`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos`
--

CREATE TABLE `productos` (
  `id` int(4) NOT NULL,
  `nombre` varchar(20) DEFAULT NULL,
  `cantidad` varchar(20) DEFAULT NULL,
  `marca` varchar(20) DEFAULT NULL,
  `categoria` varchar(40) DEFAULT NULL,
  `precio` varchar(20) DEFAULT NULL,
  `sucursal` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `productos`
--

INSERT INTO `productos` (`id`, `nombre`, `cantidad`, `marca`, `categoria`, `precio`, `sucursal`) VALUES
(3, 'Caguama', '1.2L', 'Indio', 'cerveza', '38', 'Bodega Aurrera'),
(4, 'Caguama', '1.2L', 'Indio', 'cerveza', '35', 'Bodega Bachoco'),
(5, 'Caguama', '1.2L', 'Indio', 'cerveza', '35', 'OXXO'),
(6, 'Caguama', '1.2L', 'Indio', 'cerveza', '30', 'Chedraui'),
(7, 'Tecate Ligt', '1.2L', 'Tecate', 'cerveza', '42', 'wall-mart');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sucursal`
--

CREATE TABLE `sucursal` (
  `id` int(4) NOT NULL,
  `nombre` varchar(20) DEFAULT NULL,
  `direccion` varchar(20) DEFAULT NULL,
  `horario` varchar(20) DEFAULT NULL,
  `dias` varchar(80) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `sucursal`
--

INSERT INTO `sucursal` (`id`, `nombre`, `direccion`, `horario`, `dias`) VALUES
(1, 'wall-mart', 'Tecamac Power center', '24 X 24', 'Lunes a Domingo'),
(3, 'OXXO', 'Ecatepec', '24*24', 'Lunes a domingo'),
(4, 'OXXO', 'Temascalapa', '24 X 24', 'Lunes a domingo'),
(5, 'Soriana', 'Mexico-Queretaro km.', '24 x 24', 'Toda la semana');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `id` int(4) NOT NULL,
  `nombre` varchar(50) DEFAULT NULL,
  `apellidos` varchar(60) DEFAULT NULL,
  `email` varchar(40) DEFAULT NULL,
  `password` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`id`, `nombre`, `apellidos`, `email`, `password`) VALUES
(1, 'Efren', 'Torres', 'a@gmail.com', 'abc'),
(2, 'Marco', 'Lopez', 'b@gmail.com', 'abc'),
(4, 'Alexa', 'Mena', 'c@gmail.com', 'abc'),
(8, 'Alexis', 'Luna', 'd@gmail.com', 'abc'),
(10, 'Ricardo', 'Moreno', 'e@gmail.com', 'abc'),
(12, 'Dayana', 'Diaz', 'h@gmail.com', 'abc');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `productos`
--
ALTER TABLE `productos`
  ADD KEY `id` (`id`);

--
-- Indices de la tabla `sucursal`
--
ALTER TABLE `sucursal`
  ADD KEY `id` (`id`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD UNIQUE KEY `email` (`email`),
  ADD KEY `id` (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `productos`
--
ALTER TABLE `productos`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT de la tabla `sucursal`
--
ALTER TABLE `sucursal`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
