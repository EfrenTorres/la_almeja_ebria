<!DOCTYPE html>
<?php
    session_start();
    if(empty($_SESSION['login_user']))
    {
    header('Location: index.php');
    }
?>
<html lang="zxx" class="no-js">
<head>
  <!-- Mobile Specific Meta -->
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <!-- Favicon-->
  <link rel="shortcut icon" href="imgs/almeja.png">
  <!-- Author Meta -->
  <meta name="author" content="codepixer">
  <!-- Meta Description -->
  <meta name="description" content="">
  <!-- Meta Keyword -->
  <meta name="keywords" content="">
  <!-- meta character set -->
  <meta charset="UTF-8">
  <!-- Site Title -->
  <title>Bienvenido</title>

  <link href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700" rel="stylesheet">
    <!--
    CSS
    ============================================= -->
    <link rel="stylesheet" href="css/linearicons.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/magnific-popup.css">
    <link rel="stylesheet" href="css/nice-select.css">
    <link rel="stylesheet" href="css/animate.min.css">
    <link rel="stylesheet" href="css/owl.carousel.css">
    <link rel="stylesheet" href="css/main.css">
  </head>

  <body background="imgs/a.png">

      <header id="header" id="home">
        <div class="container">
          <div class="row align-items-center justify-content-between d-flex">
            <div id="logo">
              <a href="#"><img src="imgs/almeja.png" width="120" alt="" title="" /> </a>
            </div>
            <nav id="nav-menu-container">
              <ul class="nav-menu">
                <li><h4><?php echo $_SESSION['login_user']; ?></h4></li>
                <li><a class="ticker-btn" href="logout.php">Cerrar Sesión</a></li>
                <li><a class="ticker-btn" href="configuracion.php">Administrar tu cuenta</a></li>
              </ul>
            </nav><!-- #nav-menu-container -->
          </div>
        </div>
      </header><!-- #header -->


    <!-- start banner Area -->
    <section class="banner-area relative" id="home">
      <div class="overlay overlay-bg"></div>
      <div class="container">
        <div class="row fullscreen d-flex align-items-center justify-content-center">
          <div class="banner-content col-lg-12">
            <h1 class="text-white">
              <span>Bienvenido a </span>La Almeja Ebria
            </h1>
            <p class="text-white"> <span><font size=4>Tu invetario personal,</span> A tu alcance</p></font>
          </div>
        </div>
      </div>
    </section>
    <!-- End banner Area -->

    <!-- Start features Area -->
    <section class="features-area">
      <div class="container">

        <div class="row">
          <div class="col-lg-3 col-md-6">
            <div class="single-feature">
              <h4>Jack Daniel's</h4>
                <img src="imgs/in1.png" width="100">
            </div>
          </div>
          <div class="col-lg-3 col-md-6">
            <div class="single-feature">
              <h4>Don Julio</h4>
              <img src="imgs/in2.jpg" width="100">
            </div>
          </div>
          <div class="col-lg-3 col-md-6">
            <div class="single-feature">
              <h4>Absolut</h4>
              <img src="imgs/in3.jpg" width="100">
            </div>
          </div>
          <div class="col-lg-3 col-md-6">
            <div class="single-feature">
              <h4>Buchanan's</h4>
              <img src="imgs/in4.jpg" width="100">
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- End features Area -->
    <ul class="nav-menu">
      <li>
    <hr>
    <br>
    <h2>Productos</h2>
    <br>
      <?php
      // Te recomiendo utilizar esta conección, la que utilizas ya no es la recomendada.
      $link = new PDO('mysql:host=localhost;dbname=bdweb', 'root', ''); // el campo vaciío es para la password.

      ?>

      <table class="table table-striped">

      		<thead>
      		<tr>
            <th>ID</th>
      			<th>NOMBRE</th>
      			<th>CANTIDAD</th>
      			<th>MARCA</th>
        		<th>CATEGORIA</th>
        		<th>PRECIO</th>
        		<th>SUCURSAL</th>

      		</tr>
      		</thead>
      <?php foreach ($link->query('SELECT * from productos') as $row){ // aca puedes hacer la consulta e iterarla con each. ?>
      <tr>
      	<td><?php echo $row['id'] // aca te faltaba poner los echo para que se muestre el valor de la variable.  ?></td>
        <td><?php echo $row['nombre'] ?></td>
        <td><?php echo $row['cantidad'] ?></td>
        <td><?php echo $row['marca'] ?></td>
        <td><?php echo $row['categoria'] ?></td>
        <td><?php echo $row['precio'] ?></td>
        <td><?php echo $row['sucursal'] ?></td>
       </tr>
      <?php
      	}
      ?>
      </table>

      <hr></li>
    </ul>
    <br>
    <!--actualizar-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <html lang="en" dir="ltr">
      <head>
        <meta charset="utf-8">
        <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.min.js"></script>
    <link href="css/jqueryui.css" type="text/css" rel="stylesheet"/>
        <title></title>
      </head>
      <link href="librerias/login-box.css" rel="stylesheet" type="text/css" />
    </ul>
    <ul class="nav-menu">
      <li>
      <h2>Sucursales</h2>
      <br>

        <?php
        // Te recomiendo utilizar esta conección, la que utilizas ya no es la recomendada.
        $link = new PDO('mysql:host=localhost;dbname=bdweb', 'root', ''); // el campo vaciío es para la password.

        ?>

        <table class="table table-striped">

        		<thead>
        		<tr>
              <th>ID</th>
        			<th>NOMBRE</th>
        			<th>DIRECCION</th>
        			<th>HORARIO</th>
          		<th>DIAS</th>

        		</tr>
        		</thead>
        <?php foreach ($link->query('SELECT * from sucursal') as $row){ // aca puedes hacer la consulta e iterarla con each. ?>
        <tr>
        	<td><?php echo $row['id'] // aca te faltaba poner los echo para que se muestre el valor de la variable.  ?></td>
          <td><?php echo $row['nombre'] ?></td>
          <td><?php echo $row['direccion'] ?></td>
          <td><?php echo $row['horario'] ?></td>
          <td><?php echo $row['dias'] ?></td>
         </tr>
        <?php
        	}
        ?>
        </table>
      </li>
    </ul>

    <!-- Start callto-action Area -->
    <br>
  <hr>
  <ul>
    <li>

</ul>
<li>
  <hr>
  <hr>
  <a href="../" style="margin-left:170px;"><img src="imgs/b6.png" width="120"></a></span>
  <br><br>

    <script src="js/vendor/jquery-2.2.4.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="js/vendor/bootstrap.min.js"></script>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBhOdIF3Y9382fqJYt5I_sswSrEw5eihAA"></script>
      <script src="js/easing.min.js"></script>
    <script src="js/hoverIntent.js"></script>
    <script src="js/superfish.min.js"></script>
    <script src="js/jquery.ajaxchimp.min.js"></script>
    <script src="js/jquery.magnific-popup.min.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/jquery.sticky.js"></script>
    <script src="js/jquery.nice-select.min.js"></script>
    <script src="js/parallax.min.js"></script>
    <script src="js/mail-script.js"></script>
    <script src="js/main.js"></script>
  </body>
</html>
