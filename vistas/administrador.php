<html lang="zxx" class="no-js">
<head>
  <!-- Mobile Specific Meta -->
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <!-- Favicon-->
  <link rel="shortcut icon" href="imgs/almeja.png">
  <!-- Author Meta -->
  <meta name="author" content="codepixer">
  <!-- Meta Description -->
  <meta name="description" content="">
  <!-- Meta Keyword -->
  <meta name="keywords" content="">
  <!-- meta character set -->
  <meta charset="UTF-8">
  <!-- Site Title -->

  <link href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700" rel="stylesheet">
    <!--
    CSS
    ============================================= -->
    <link rel="stylesheet" href="css/linearicons.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/magnific-popup.css">
    <link rel="stylesheet" href="css/nice-select.css">
    <link rel="stylesheet" href="css/animate.min.css">
    <link rel="stylesheet" href="css/owl.carousel.css">
    <link rel="stylesheet" href="css/main.css">
  </head>
  <body background="imgs/wp.jpg">

      <header id="header" id="home">
        <div class="container">
          <div class="row align-items-center justify-content-between d-flex">
            <div id="logo">
              <a href="#"><img src="imgs/almeja.png" width="120" alt="" title="" /> </a>
            </div>
            <nav id="nav-menu-container">
              <ul class="nav-menu">
                <li><h4>Administrador</h4></li>
                <li><a class="ticker-btn" href="index.php">Salir</a></li>
              </ul>
            </nav><!-- #nav-menu-container -->
          </div>
        </div>
      </header><!-- #header -->

    <!-- start banner Area -->
    <br><br><br><br><br><br>

    <!-- Start features Area -->
    <!-- End features Area -->
    <ul class="nav-menu">
      <li>

    <br>
    <h2>Productos</h2>
    <br>
      <?php
      // Te recomiendo utilizar esta conección, la que utilizas ya no es la recomendada.
      $link = new PDO('mysql:host=localhost;dbname=bdweb', 'root', ''); // el campo vaciío es para la password.

      ?>

      <table class="table table-striped">

      		<thead>
      		<tr>
            <th>ID</th>
      			<th>NOMBRE</th>
      			<th>CANTIDAD</th>
      			<th>MARCA</th>
        		<th>CATEGORIA</th>
        		<th>PRECIO</th>
        		<th>SUCURSAL</th>

      		</tr>
      		</thead>
      <?php foreach ($link->query('SELECT * from productos') as $row){ // aca puedes hacer la consulta e iterarla con each. ?>
      <tr>
      	<td><?php echo $row['id'] // aca te faltaba poner los echo para que se muestre el valor de la variable.  ?></td>
        <td><?php echo $row['nombre'] ?></td>
        <td><?php echo $row['cantidad'] ?></td>
        <td><?php echo $row['marca'] ?></td>
        <td><?php echo $row['categoria'] ?></td>
        <td><?php echo $row['precio'] ?></td>
        <td><?php echo $row['sucursal'] ?></td>
       </tr>
      <?php
      	}
      ?>
      </table>

      <hr></li>
    </ul>
    <br>
    <!--actualizar-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <html lang="en" dir="ltr">
      <head>
        <meta charset="utf-8">
        <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.min.js"></script>
    <link href="css/jqueryui.css" type="text/css" rel="stylesheet"/>
        <title></title>
      </head>
      <link href="librerias/login-box.css" rel="stylesheet" type="text/css" />
      <!--MODIFICAR DATOS -->
    <ul class="nav-menu">
      <li>
      <h2>Acciones de Productos</h2>
      <br>
      <form>
    		<input class="form-login" size="50" maxlength="4048" type="text" id="id" placeholder="Buscar ID" value=""/> <br/>
    		<br>
    		<input class="form-login" size="50" maxlength="4048" type="text" id="nombre" placeholder="nombre" value=""/>
    		<br><br>
    		<input class="form-login" size="50" maxlength="4048" type="text" id="cantidad" placeholder="cantidad" value=""/>
    		<br><br>
    		<input class="form-login" size="50" maxlength="4048" type="text" id="marca" placeholder="marca" value=""/>
        <br><br>
    		<input class="form-login" size="50" maxlength="4048" type="text" id="categoria" placeholder="categoria" value=""/>
        <br><br>
    		<input class="form-login" size="50" maxlength="4048" type="text" id="precio" placeholder="precio" value=""/>
        <br><br>
    		<input class="form-login" size="50" maxlength="4048" type="text" id="sucursal" placeholder="sucursal" value=""/>
        <br><b><div id="error"></div></b><br>
        <input type="image" src="imgs/guardar.png" width="140" height="55" id="actualizar"><input type="image" src="imgs/eliminar.png" width="140" height="55" id="eliminar">
        <br>
        <input type="image" src="imgs/agregar.png" width="280" height="55" id="agregar">

        <hr>
    	</form>
      </li>
      <!--Ajax-->

        <script src="librerias/jquery.min.js"></script>
        <script src="librerias/jquery.ui.shake.js"></script>
      <script type="application/javascript">

        //datos de la bd
        $(document).ready(function(){

            //busqueda
                  $("#id").focusout(function(){
              $.ajax({
                    url:'productosM.php',
                  type:'get',
                  dataType:'json',
                  data:{ id:$('#id').val()}
              }).done(function(respuesta){
                  $("#nombre").val(respuesta.nombre);
                  $("#cantidad").val(respuesta.cantidad);
                  $("#marca").val(respuesta.marca);
                  $("#categoria").val(respuesta.categoria);
                  $("#precio").val(respuesta.precio);
                  $("#sucursal").val(respuesta.sucursal);
              });
            });

        });
        <!--actualizar-->
        $(document).ready(function()
        {

        $('#actualizar').click(function()
        {
        var id=$("#id").val();
        var nombre=$("#nombre").val();
        var cantidad=$("#cantidad").val();
        var marca=$("#marca").val();
        var categoria=$("#categoria").val();
        var precio=$("#precio").val();
        var sucursal=$("#sucursal").val();
        var dataString = 'id='+id+'&nombre='+nombre+'&cantidad='+cantidad+'&marca='+marca
                          +'&categoria='+categoria+'&precio='+precio+'&sucursal='+sucursal;
        if( $.trim(id).length>0 && $.trim(nombre).length>0 && $.trim(cantidad).length>0 && $.trim(marca).length>0
          && $.trim(categoria).length>0 && $.trim(precio).length>0 && $.trim(sucursal).length>0)
        {
        $.ajax({
        type: "GET",
        url: "../control.php/?controller=producto&action=actualizar",
        data: dataString,
        cache: false,
        success: function(data){
        if(data)
        {
          $("#error").html("<span style='color:#cc0000'>Información: </span> "+data);
        }
        else
        {
        $("#error").html("<span style='color:#cc0000'>Error:</span> ... ");
        }
        }
        });

        }
        return false;
        });

        });
        <!--Eliminar-->
        $(document).ready(function()
        {

        $('#eliminar').click(function()
        {
        var id=$("#id").val();
        var dataString = 'id='+id;
        if( $.trim(id).length>0)
        {
        $.ajax({
        type: "GET",
        url: "../control.php/?controller=producto&action=eliminar",
        data: dataString,
        cache: false,
        beforeSend: function(){ $("#actualizar").val('Connecting...');},
        success: function(data){
        if(data)
        {
          $("#error").html("<span style='color:#cc0000'>Información: </span> "+data);
        }
        else
        {
        $("#error").html("<span style='color:#cc0000'>Error:</span> ... ");
        }
        }
        });

        }
        return false;
        });

        });
        <!--Agregar-->
        $(document).ready(function()
        {

        $('#agregar').click(function()
        {
        var nombre=$("#nombre").val();
        var cantidad=$("#cantidad").val();
        var marca=$("#marca").val();
        var categoria=$("#categoria").val();
        var precio=$("#precio").val();
        var sucursal=$("#sucursal").val();
        var dataString = 'nombre='+nombre+'&cantidad='+cantidad+'&marca='+marca
                          +'&categoria='+categoria+'&precio='+precio+'&sucursal='+sucursal;
        if($.trim(nombre).length>0 && $.trim(cantidad).length>0 && $.trim(marca).length>0
          && $.trim(categoria).length>0 && $.trim(precio).length>0 && $.trim(sucursal).length>0)
        {
        $.ajax({
        type: "GET",
        url: "../control.php/?controller=producto&action=agregar",
        data: dataString,
        cache: false,
        success: function(data){
        if(data)
        {
          $("#error").html("<span style='color:#cc0000'>Información: </span> "+data);
        }
        else
        {
        }
        }
        });

        }
        return false;
        });

        });
      </script>


    </ul>
    <ul class="nav-menu">
      <li>
      <h2>Sucursales</h2>
      <br>

        <?php
        // Te recomiendo utilizar esta conección, la que utilizas ya no es la recomendada.
        $link = new PDO('mysql:host=localhost;dbname=bdweb', 'root', ''); // el campo vaciío es para la password.

        ?>

        <table class="table table-striped">

        		<thead>
        		<tr>
              <th>ID</th>
        			<th>NOMBRE</th>
        			<th>DIRECCION</th>
        			<th>HORARIO</th>
          		<th>DIAS</th>

        		</tr>
        		</thead>
        <?php foreach ($link->query('SELECT * from sucursal') as $row){ // aca puedes hacer la consulta e iterarla con each. ?>
        <tr>
        	<td><?php echo $row['id'] // aca te faltaba poner los echo para que se muestre el valor de la variable.  ?></td>
          <td><?php echo $row['nombre'] ?></td>
          <td><?php echo $row['direccion'] ?></td>
          <td><?php echo $row['horario'] ?></td>
          <td><?php echo $row['dias'] ?></td>
         </tr>
        <?php
        	}
        ?>
        </table>
      </li>
    </ul>

    <!-- Start callto-action Area -->
    <br>
  <hr>
  <ul>
    <li><ul class="nav-menu">
      <li>
      <h2>Modificaciones</h2>
      <br>
      <form>
    		<input class="form-login" size="50" maxlength="4048" type="text" id="ids" placeholder="Buscar ID" value=""/> <br/>
    		<br>
    		<input class="form-login" size="50" maxlength="4048" type="text" id="nombres" placeholder="nombre" value=""/>
    		<br><br>
    		<input class="form-login" size="50" maxlength="4048" type="text" id="direccions" placeholder="direccion" value=""/>
    		<br><br>
    		<input class="form-login" size="50" maxlength="4048" type="text" id="horarios" placeholder="horario" value=""/>
        <br><br>
    		<input class="form-login" size="50" maxlength="4048" type="text" id="diass" placeholder="dias" value=""/>
        <br><br><b><div id="errors"></div></b><br>
    		<input type="image" src="imgs/guardar.png" width="140" height="55" id="actualizars"><input type="image" src="imgs/eliminar.png" width="140" height="55" id="eliminars">
        <br>
        <input type="image" src="imgs/agregar.png" width="280" height="55" id="agregars">
        <hr>
    	</form>
      </li>
      <!--Ajax-->

        <script src="librerias/jquery.min.js"></script>
        <script src="librerias/jquery.ui.shake.js"></script>
      <script type="application/javascript">

        //datos de la bd
        $(document).ready(function(){
              $("#ids").focusout(function(){
              $.ajax({
                    url:'sucursalM.php',
                  type:'get',
                  dataType:'json',
                  data:{ id:$('#ids').val()}
              }).done(function(respuesta){
                  $("#nombres").val(respuesta.nombre);
                  $("#direccions").val(respuesta.direccion);
                  $("#horarios").val(respuesta.horario);
                  $("#diass").val(respuesta.dias);
              });
            });

        });
        <!--actualizar-->
        $(document).ready(function()
        {

        $('#actualizars').click(function()
        {
        var id=$("#ids").val();
        var nombre=$("#nombres").val();
        var direccion=$("#direccions").val();
        var horario=$("#horarios").val();
        var dias=$("#diass").val();
        var dataString = 'id='+id+'&nombre='+nombre+'&direccion='+direccion+'&horario='+horario+'&dias='+dias;
        if( $.trim(id).length>0 && $.trim(nombre).length>0 && $.trim(direccion).length>0 && $.trim(horario).length>0 && $.trim(dias).length>0)
        {
        $.ajax({
        type: "get",
        url: "../control.php/?controller=sucursal&action=actualizar",
        data: dataString,
        cache: false,
        success: function(data){
        if(data)
        {
          $("#errors").html("<span style='color:#cc0000'>Información: </span> "+data);
        }
        else
        {
        $("#errors").html("<span style='color:#cc0000'>Error:</span> ... ");
        }
        }
        });

        }
        return false;
        });

        });
        <!--Eliminar-->
        $(document).ready(function()
        {

        $('#eliminars').click(function()
        {
        var id=$("#ids").val();
        var dataString = 'id='+id;
        if( $.trim(id).length>0)
        {
        $.ajax({
        type: "get",
        url: "../control.php/?controller=sucursal&action=eliminar",
        data: dataString,
        cache: false,
        success: function(data){
        if(data)
        {
          $("#errors").html("<span style='color:#cc0000'>Información: </span> "+data);
        }
        else
        {
        $("#errors").html("<span style='color:#cc0000'>Error:</span> ... ");
        }
        }
        });

        }
        return false;
        });

        });
        <!--Agregar-->
        $(document).ready(function()
        {

        $('#agregars').click(function()
        {
        var id=$("#ids").val();
        var nombre=$("#nombres").val();
        var direccion=$("#direccions").val();
        var horario=$("#horarios").val();
        var dias=$("#diass").val();
        var dataString = 'id='+id+'&nombre='+nombre+'&direccion='+direccion+'&horario='+horario+'&dias='+dias;
        if( $.trim(id).length>0 && $.trim(nombre).length>0 && $.trim(direccion).length>0 && $.trim(horario).length>0 && $.trim(dias).length>0)
        {
        $.ajax({
        type: "get",
        url: "../control.php/?controller=sucursal&action=agregar",
        data: dataString,
        cache: false,
        beforeSend: function(){ $("#agregars").val('Connecting...');},
        success: function(data){
        if(data)
        {
          $("#errors").html("<span style='color:#cc0000'>Información: </span> "+data);
        }
        else
        {
        }
        }
        });

        }
        return false;
        });

        });
      </script>

    </li>
</ul>

<a href="../#" style="margin-left:170px;"><img src="imgs/b6.png" width="120"></a></span>
    <script src="js/vendor/jquery-2.2.4.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="js/vendor/bootstrap.min.js"></script>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBhOdIF3Y9382fqJYt5I_sswSrEw5eihAA"></script>
      <script src="js/easing.min.js"></script>
    <script src="js/hoverIntent.js"></script>
    <script src="js/superfish.min.js"></script>
    <script src="js/jquery.ajaxchimp.min.js"></script>
    <script src="js/jquery.magnific-popup.min.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/jquery.sticky.js"></script>
    <script src="js/jquery.nice-select.min.js"></script>
    <script src="js/parallax.min.js"></script>
    <script src="js/mail-script.js"></script>
    <script src="js/main.js"></script>
  </body>
</html>
