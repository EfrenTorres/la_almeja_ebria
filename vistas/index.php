<?php
session_start();
if(!empty($_SESSION['login_user']))
{
header('Location: home.php');
}
?>
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<link href="librerias/login-box.css" rel="stylesheet" type="text/css" />
<body background="imgs/bg.gif">

<center>
<img src="imgs/almeja.png" width="350px">
<div id="login-box">
  <h2>Inicio de Sesión</h2>
  <br>
<form action="" method="post">
  <div id="login-box-field" style="margin-top:20px;">
  <input type="email" class="form-login" id="email" placeholder="Email" size="30" maxlength="2048">
  </div>
  <br>
  <br>
  <div id="login-box-field">
  <input type="password" class="form-login" id="password" placeholder="Contrase&ntilde;a" size="30" maxlength="2048">
  </div>
  <br><br>
  <a href="registrarse.php" style="margin-left:190px;">Registrate Aquí</a></span>
  <br><br>
  <input type="image" src="imgs/login.png" width="180" height="40" id="login">
  <br><br>
  <div id="error"></div>
  <hr>
  <a href="recuperar.php" style="margin-left:170px;">Recuperar contraseña</a></span>
  <br><br>
  <a href="administrador.php"><img src="imgs/admin.png" width="150"></a>

</form>
</div>

<script src="librerias/jquery.min.js"></script>
<script src="librerias/jquery.ui.shake.js"></script>
<script>
$(document).ready(function()
{

$('#login').click(function()
{
var email=$("#email").val();
var password=$("#password").val();
var dataString = 'email='+email+'&password='+password;
if($.trim(email).length>0 && $.trim(password).length>0)
{
$.ajax({
type: "GET",
url: "../control.php/?controller=usuario&action=login",
data: dataString,
cache: false,
success: function(data){
if(data)
{
$("body").load("home.php").hide().fadeIn(1500).delay(6000);
//or
var email=$("#email").val();
window.location.href = "home.php";
}
else
{
//Shake animation effect.
$('#box').shake();
$("#login").val('Login')
$("#error").html("<span style='color:#cc0000'>Error:</span> Usuario o contraseña invalidos. ");
}
}
});

}
return false;
});

});
</script>
<hr>
<a href="../#" style="margin-left:170px;"><img src="imgs/b6.png" width="120"></a></span>
<br><br>
