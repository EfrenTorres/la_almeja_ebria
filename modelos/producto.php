<?php
namespace web\modelos;
class producto extends conexion{
public $id;
public $nombre;
public $cantidad;
public $marca;
public $categoria;
public $precio;
public $sucursal;
    function agregar(){
      $pre = mysqli_prepare($this->con, "INSERT INTO productos(nombre, cantidad, marca, categoria, precio, sucursal) VALUES (?,?,?,?,?,?)");
      $pre->bind_param("ssssss",$this->nombre, $this->cantidad, $this->marca, $this->categoria, $this->precio, $this->sucursal);
      $pre->execute();
      if (mysqli_affected_rows($this->con)>0){
        echo json_encode("El producto ha sido creado");
      }
    }
    function actualizar(){
      $pre = mysqli_prepare($this->con, "UPDATE productos SET nombre='$this->nombre',cantidad='$this->cantidad', marca='$this->marca',
                                        categoria='$this->categoria',precio='$this->precio',sucursal='$this->sucursal' WHERE id='$this->id'");
      $pre->execute();
      if (mysqli_affected_rows($this->con)>0){
        echo json_encode("La informacion a sido actualizada");
      }else{
       echo json_encode("Error al actualizar");
      }
    }

    function eliminar(){
        $id=$_GET["id"];
        $pre = mysqli_prepare($this->con, "DELETE FROM productos WHERE id=?");
        $pre->bind_param("i",$id);
        $pre->execute();
        if (mysqli_affected_rows($this->con)>0){
          echo json_encode("Producto Eliminado");
        }else{
         echo json_encode("Error al eliminar");
        }
    }
}

?>
