<?php
  use web\modelos\usuario;
  include "modelos/conexion.php";
  include "modelos/usuario.php";
    class usuarioController{

      function login(){
        if (isset($_GET)) {
            $usuario = new Usuario();
  				  $usuario->email=$_GET['email'];
  				  $usuario->password=$_GET['password'];
  				  $usuario->login();
        }else {
          echo json_encode("FALTAN DATOS");
        }
      }
      function mostrarDatos(){
        if (isset($_GET)) {
            $usuario = new Usuario();
  				  $usuario->email=$_GET['email'];
  				  $usuario->mostrarDatos();
        }else {
          echo json_encode("FALTAN DATOS");
        }
      }
      function crearUsuario(){
        if (isset($_GET)) {
            $usuario = new Usuario();
            $usuario->nombre=$_GET['nombre'];
            $usuario->apellidos=$_GET['apellidos'];
  				  $usuario->email=$_GET['email'];
  				  $usuario->password=$_GET['password'];
  				  $usuario->crearUsuario();
        }else {
          echo json_encode("FALTAN DATOS");
        }
      }
      function actualizarUsuario(){
        if (isset($_GET)) {
            $usuario = new Usuario();
            $usuario->nombre=$_GET['nombre'];
            $usuario->apellidos=$_GET['apellidos'];
  				  $usuario->email=$_GET['email'];
  				  $usuario->password=$_GET['password'];
  				  $usuario->actualizarUsuario();
        }else {
          echo json_encode("FALTAN DATOS");
        }
      }
      function mostrarUsuario(){
        if (isset($_GET)) {
            $usuario = new Usuario();
  				  $usuario->email=$_GET['email'];
  				  $usuario->mostrarUsuario();
        }else {
          echo json_encode("FALTAN DATOS");
        }
      }
      function recuperarPassword(){
        if (isset($_GET)) {
            $usuario = new Usuario();
            $usuario->nombre=$_GET['nombre'];
            $usuario->apellidos=$_GET['apellidos'];
            $usuario->email=$_GET['email'];
            $usuario->recuperarPassword();
        }else {
          echo json_encode("FALTAN DATOS");
        }
      }
    }
 ?>
