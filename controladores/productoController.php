<?php
  use web\modelos\producto;
  include "modelos/conexion.php";
  include "modelos/producto.php";
    class productoController{

      function agregar(){
        if (isset($_GET)) {
            $producto = new producto();
            $producto->nombre=$_GET['nombre'];
            $producto->cantidad=$_GET['cantidad'];
  				  $producto->marca=$_GET['marca'];
  				  $producto->categoria=$_GET['categoria'];
  				  $producto->precio=$_GET['precio'];
  				  $producto->sucursal=$_GET['sucursal'];
  				  $producto->agregar();
        }else {
          echo json_encode("FALTAN DATOS");
        }
      }
        function actualizar(){
          if (isset($_GET)) {
              $producto = new producto();
              $producto->id=$_GET['id'];
              $producto->nombre=$_GET['nombre'];
              $producto->cantidad=$_GET['cantidad'];
    				  $producto->marca=$_GET['marca'];
    				  $producto->categoria=$_GET['categoria'];
    				  $producto->precio=$_GET['precio'];
    				  $producto->sucursal=$_GET['sucursal'];
    				  $producto->actualizar();
          }else {
            echo json_encode("FALTAN DATOS");
          }
      }
      function eliminar(){
        if (isset($_GET)) {
            $producto = new producto();
            $producto->id=$_GET['id'];
            $producto->eliminar();
        }else {
          echo json_encode("FALTAN DATOS");
        }
      }
    }
 ?>
