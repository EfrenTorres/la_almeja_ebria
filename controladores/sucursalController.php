<?php
  use web\modelos\sucursal;
  include "modelos/conexion.php";
  include "modelos/sucursal.php";
    class sucursalController{

      function agregar(){
        if (isset($_GET)) {
            $sucursal = new sucursal();
            $sucursal->nombre=$_GET['nombre'];
            $sucursal->direccion=$_GET['direccion'];
  				  $sucursal->horario=$_GET['horario'];
  				  $sucursal->dias=$_GET['dias'];
  				  $sucursal->agregar();
        }else {
          echo json_encode("FALTAN DATOS");
        }
      }
        function actualizar(){
          if (isset($_GET)) {
              $sucursal = new sucursal();
              $sucursal->id=$_GET['id'];
              $sucursal->nombre=$_GET['nombre'];
              $sucursal->direccion=$_GET['direccion'];
    				  $sucursal->horario=$_GET['horario'];
    				  $sucursal->dias=$_GET['dias'];
    				  $sucursal->actualizar();
          }else {
            echo json_encode("FALTAN DATOS");
          }
      }
      function eliminar(){
        if (isset($_GET)) {
            $sucursal = new sucursal();
            $sucursal->id=$_GET['id'];
            $sucursal->eliminar();
        }else {
          echo json_encode("FALTAN DATOS");
        }
      }
    }
 ?>
